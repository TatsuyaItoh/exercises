import java.util.Scanner;
public class ExercisesInClass2 {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);

        String[] a =new String[3];
        a[0]="Tempra";
        a[1]="Ramen";
        a[2]="Udon";

        System.out.println("What would you like to order:");
        for(int i=0;i<3;i++) {
            System.out.println((i+1)+". " + a[i]);
        }

        System.out.print("Your Order [1-3]:");
        int number = userIn.nextInt();
        userIn.close();

        printMassage(a[number-1]);
    }
    static void printMassage(String FoodName){
        System.out.println("you have ordered "+FoodName+" Thank you!");
    }
}
