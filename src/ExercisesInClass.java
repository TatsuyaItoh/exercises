import java.util.Scanner;
public class ExercisesInClass {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);

        String[] a =new String[3];
        a[0]="Tempra";
        a[1]="Ramen";
        a[2]="Udon";

        System.out.println("What would you like to order:");
        for(int i=0;i<3;i++) {
            System.out.println((i+1)+". " + a[i]);
        }

        System.out.println("Your Order [1-3]:");
        int number = userIn.nextInt();
        userIn.close();

        System.out.println("We have received your order for "+a[number-1]+"! It will be served soon!");
    }
}
